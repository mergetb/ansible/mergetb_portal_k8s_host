#!/bin/env bash
#
# this script exists per issue:
# https://github.com/kubernetes/kubernetes/issues/106957
#
CGROUPDIRS=($(find /sys/fs/cgroup -type d))
LEFTOVER_SCOPES_TMP=($(journalctl --since "10m ago" | egrep 'Failed to update stats for container|Failed to create existing container' | grep -o 'crio-.*scope' | sort -u))

for PODID in `journalctl --since "10 ago" | grep 'Unable to fetch pod log stats' | grep -o '\/var.*:' | tr -d ':' | awk -F_ '{print $NF}' | sed 's/-/_/g' | sort -u`;
do
    LEFTOVER_SCOPES_TMP+=($(printf -- '%s\n' "${testarray[@]}" | grep ${PODID} | grep -o crio.*$))
done

LEFTOVER_SCOPES=($(printf -- '%s\n' "${LEFTOVER_SCOPES_TMP[@]}" | sort -u))

POD_IDS=($(crictl pods -q))
POD_SCOPES=()

for POD_ID in ${POD_IDS[@]};
do
    JSONDUMP="`crictl inspectp ${POD_ID}`"
    POD_SCOPE="`echo ${JSONDUMP} | jq -r '.info.runtimeSpec.linux.cgroupsPath' | awk -F: '{print "crio-" $NF ".scope"}'`"
    if [[ $? -ne 0 || -z "${POD_SCOPE}" ]];
    then
        echo "Error fetching pod SCOPE for pod with ID ${POD_ID}"
        continue
    else
        POD_SCOPES+=($POD_SCOPE)
    fi
done

for SCOPENAME in ${LEFTOVER_SCOPES[@]}; do
    if [[ " ${POD_SCOPES[*]} " =~ " ${SCOPENAME} " ]];
    then
        echo "Scope ${SCOPENAME} found under running pod, skipping..."
        continue
    else
        for SCOPE in `printf -- '%s\n' "${CGROUPDIRS[@]}" | grep ${SCOPENAME}`;
        do
            echo "Removing CGROUP ${SCOPENAME} and its parent..."
            rmdir ${SCOPE}
            if [[ $? -eq 0 ]];
            then
                rmdir `dirname ${SCOPE}`
                if [[ $? -ne 0 ]];
                then
                    echo "Failed to remove parent for CGROUP ${SCOPE}..."
                fi
            fi
        done
    fi
done
